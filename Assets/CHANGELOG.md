# Changelog
All notable changes to this project will be documented in this file.

## [2.0.5] - 20-01-2020
### Changed
	-update version v2.0.335
	-downgrade version `system-runtime-compiler-services-unsafe` from 4.7.0 to 4.5.2
	-add window generate
	-mpc.exe now inlcuded relativepath/GeneratorTools/win-x64/mpc.exe


