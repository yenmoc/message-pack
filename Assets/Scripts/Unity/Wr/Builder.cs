﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

#if UNITY_EDITOR
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEditor;

public static partial class Builder
{
    private const string MESSAGE_PACK_GENERATOR = "/GeneratorTools";
    private const string MESSAGE_PACK_GENERATE_OUTPUT = "/Root/Scripts/Database/Generated/MessagePack.Generated.cs";

    #region -- message pack ------------------------------------------------

    public static void GenerateMessagePack(List<string> assemblyNames)
    {
        UnityEngine.Debug.Log("MessagePack Generator : Start...");

        bool TypeExist(string assemblyName)
        {
            return System.AppDomain.CurrentDomain
                       .GetAssemblies()
                       .FirstOrDefault(assembly => assembly.GetName().Name.Equals(assemblyName)) != null;
        }

        //Check if assembly in assemblyNames exits or not
        foreach (var t in assemblyNames.Where(t => !TypeExist(t)))
        {
            EditorUtility.DisplayDialog("Assembly Not Found", $"Can not found {t} assembly!\nPlease check that the assembly name is correct!", "Ok");
            return;
        }

        var rootPath = UnityEngine.Application.dataPath + "/..";
        // ReSharper disable once RedundantAssignment
        var exeFileName = string.Empty;
#if UNITY_EDITOR_WIN
        exeFileName = "/win-x64/mpc.exe";
#elif UNITY_EDITOR_OSX
        exeFileName = "/osx-x64/mpc";
#elif UNITY_EDITOR_LINUX
        exeFileName = "/linux-x64/mpc";
#else
        return;
#endif

        var input = "";
        for (int i = 0; i < assemblyNames.Count; i++)
        {
            if (i == assemblyNames.Count - 1)
            {
                input += $"{rootPath}/{assemblyNames[i]}.csproj";
            }
            else
            {
                input += $"{rootPath}/{assemblyNames[i]}.csproj,";
            }
        }

        var psi = new ProcessStartInfo()
        {
            CreateNoWindow = true,
            WindowStyle = ProcessWindowStyle.Hidden,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            FileName = Path.GetFullPath("Packages/com.yenmoc.message-pack") + MESSAGE_PACK_GENERATOR + exeFileName,
            Arguments = $@"-i ""{input}"" -o ""{UnityEngine.Application.dataPath}{MESSAGE_PACK_GENERATE_OUTPUT}""",
        };

        var p = Process.Start(psi);
        if (p == null) return;
        p.EnableRaisingEvents = true;
        p.Exited += (sender, e) =>
        {
            var data = p.StandardOutput.ReadToEnd();
            UnityEngine.Debug.Log($"{data}");
            UnityEngine.Debug.Log("MessagePack Generator : Complete!");
            p?.Dispose();
            p = null;
        };

        void AddDefineSymbols(params string[] symbols)
        {
            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            var allDefines = definesString.Split(';').ToList();
            allDefines.AddRange(symbols.Except(allDefines));
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, string.Join(";", allDefines.ToArray()));
            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        AddDefineSymbols("MESSAGE_PACK");
    }

    #endregion
}
#endif