﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

#if UNITY_EDITOR
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class MessagePackGeneratorWindow : EditorWindow
{
    public const string SAVE_ASSEMBLY_KEY = "assembly_key";

    /// <summary>
    /// List of table names which want to download
    /// </summary>
    [SerializeField] private List<string> assemblyNames = new List<string>();

    private readonly Color _yellow = new Color32(255, 255, 0, 255);
    private readonly Color _green = new Color32(124, 252, 0, 255);

    private void OnGUI()
    {
        var removeId = -1;
        EditorGUILayout.HelpBox("These assembly below will be generated", MessageType.Info);
        EditorGUILayout.HelpBox("You can not selected Assembly-CSharp.csproj, so class need serialize and deserialize with message pack should be placed in another assembly.", MessageType.Info);
        GUILayout.Space(8);
        var r = EditorGUILayout.GetControlRect(GUILayout.Height(11));
        r.height = 1;
        r.y += 10 / 2f;
        r.x -= 2f;
        r.width += 6f;
        EditorGUI.DrawRect(r, _yellow);
        GUILayout.Label("Assembly names", EditorStyles.boldLabel);
        if (EditorPrefs.HasKey(SAVE_ASSEMBLY_KEY) && assemblyNames.Count == 0)
        {
            assemblyNames.Clear();
            var count = EditorPrefs.GetInt(SAVE_ASSEMBLY_KEY);
            for (int i = 0; i < count; i++)
            {
                assemblyNames.Add(EditorPrefs.GetString(SAVE_ASSEMBLY_KEY + i));
            }
        }

        for (var i = 0; i < assemblyNames.Count; i++)
        {
            GUILayout.BeginHorizontal();
            assemblyNames[i] = EditorGUILayout.TextField($"assembly {i}", assemblyNames[i]);
            // button search assembly
            GUI.backgroundColor = _yellow;
            if (GUILayout.Button("Find", EditorStyles.miniButton, GUILayout.Width(38)))
            {
                var path = EditorUtility.OpenFilePanel("Select Assembly Name", Directory.GetCurrentDirectory(), "csproj");
                if (path.Length != 0)
                {
                    assemblyNames[i] = Path.GetFileNameWithoutExtension(path);
                }
            }

            GUI.backgroundColor = Color.white;

            // button remove assembly name
            if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(20)))
            {
                removeId = i;
            }

            GUILayout.EndHorizontal();
        }

        if (removeId >= 0) assemblyNames.RemoveAt(removeId);

        if (GUILayout.Button("Add assembly name", GUILayout.Width(130)))
        {
            assemblyNames.Add("");
        }

        if (assemblyNames.Count > 0)
        {
            GUILayout.Space(8);
            GUI.backgroundColor = _green;
            if (GUILayout.Button("Generate Message Pack"))
            {
                Builder.GenerateMessagePack(assemblyNames);
                EditorPrefs.SetInt(SAVE_ASSEMBLY_KEY, assemblyNames.Count);
                for (int i = 0; i < assemblyNames.Count; i++)
                {
                    EditorPrefs.SetString(SAVE_ASSEMBLY_KEY + i, assemblyNames[i]);
                }
            }

            GUI.backgroundColor = Color.white;
        }
    }

    [MenuItem("Window/MessagePack/[Generate]")]
    public static void ShowWindow()
    {
        var window = GetWindow(typeof(MessagePackGeneratorWindow));
        window.titleContent = new GUIContent("MessagePack Generate");
        window.minSize = new Vector2(500, 250);
    }
}
#endif